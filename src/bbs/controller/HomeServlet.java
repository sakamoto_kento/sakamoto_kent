
package bbs.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.User;
import bbs.service.HomeService;

@WebServlet(urlPatterns={"/index.jsp"})
public class HomeServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {
		//ユーザ一覧情報のリスト
		List<User>	users = new HomeService().getUsers();

		request.setAttribute("users", users);
		request.getRequestDispatcher("/home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		List<User> users = new HomeService().getUsers();
		String account_status = request.getParameter("account_status");
		String id = request.getParameter("id");

		new HomeService().changeStatus(account_status,id);
		HttpSession session = request.getSession();
		session.setAttribute("users", users);
		response.sendRedirect("./");
	}
}
