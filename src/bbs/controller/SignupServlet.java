package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Postion;
import bbs.beans.User;
import bbs.service.DepartmentService;
import bbs.service.SignupService;

@WebServlet(urlPatterns = {"/signup"})
public class SignupServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException,ServletException {

			List<Branch> branch = new DepartmentService().getBranch();
			List<Postion> postion = new DepartmentService().getPostion();

			request.setAttribute("branch",branch);
			request.setAttribute("postion",postion);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException,ServletException {

		User user = new User();
		List<Branch> branch = new DepartmentService().getBranch();
		List<Postion> postion = new DepartmentService().getPostion();
		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

		if(isValid(request,messages) == true) {
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			user.setPostion_id(Integer.parseInt(request.getParameter("postion_id")));

			new SignupService().register(user);
			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages", messages);
			user.setLogin_id(request.getParameter("login_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
			user.setPostion_id(Integer.parseInt(request.getParameter("postion_id")));

			request.setAttribute("user", user);
			request.setAttribute("branch", branch);
			request.setAttribute("postion", postion);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
		}

	}

	private boolean isValid(HttpServletRequest request, List<String> errormessage) {

    	String login_id = request.getParameter("login_id");
    	String password = request.getParameter("password");
    	String passwordcheck = request.getParameter("passwordcheck");
    	String name = request.getParameter("name");
    	int branch_id = Integer.parseInt(request.getParameter("branch_id"));
    	int postion_id = Integer.parseInt(request.getParameter("postion_id"));

    	//未入力時
    	if(StringUtils.isBlank(login_id) == true) {
    		errormessage.add("ログインIDを入力してください");
    	}
    	if(StringUtils.isBlank(password) == true) {
    		errormessage.add("パスワードを入力してください");
    	}
    	if(StringUtils.isBlank(passwordcheck) == true) {
    		errormessage.add("確認用パスワードを入力してください");
    	}
    	if(StringUtils.isBlank(name) == true) {
    		errormessage.add("ユーザ名を入力してください");
    	}
    	if(StringUtils.isBlank(String.valueOf(branch_id)) == true){
    		errormessage.add("支店を入力してください");
    	}
    	if(StringUtils.isBlank(String.valueOf(postion_id)) == true){
    		errormessage.add("部署・役職を入力してください");
    	}
    	//ユーザ定義
    	//半角英数字で6文字以上20文字以下。
    	if(login_id.matches("[0-9a-zA-Z]{6,20}") != true) {
    		errormessage.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください。");
    	}
    	//記号を含む全ての半角文字で6文字以上20文字以下。
		if(!password.matches("[a-zA-Z0-9｡-ﾟ -/:-@\\[-\\`\\{-\\~]{6,20}")){
			errormessage.add("パスワードは半角文字で6文字以上20文字以下で入力してください。");
		}
    	//名称は10文字以下
    	if(name.length()<11 != true) {
    		errormessage.add("ユーザ名は10文字以下で入力してください。");
    	}
    	//パスワード比較判定
    	if(!password.equals(passwordcheck)) {
    		errormessage.add("パスワードが確認用のパスワードと一致しません。");
    	}
    	//ログインID重複時エラー（入力されログインIDをDaoまで運んで検索結果を1か0かで返して判定しています。
    	int idCheck = new SignupService().checkId(login_id);
    	if(idCheck == 1) {
    		errormessage.add("既にログインIDが利用されています。");
    	}

    	if(errormessage.size() == 0) {
    		return true;
    	} else {
    		return false;
    	}
    }
}
