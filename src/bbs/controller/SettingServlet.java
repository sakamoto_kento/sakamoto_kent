package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Postion;
import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.service.DepartmentService;
import bbs.service.EditService;

@WebServlet(urlPatterns = {"/setting"})
public class SettingServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {

		String id = request.getParameter("id");
		User editUser = new EditService().getEdit(id);
		List<Branch> branch = new DepartmentService().getBranch();
		List<Postion> postion = new DepartmentService().getPostion();

		request.setAttribute("branch",branch);
		request.setAttribute("postion",postion);
		request.setAttribute("editUser", editUser);
		request.getRequestDispatcher("/setting.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,IOException {

		List<String> messages = new ArrayList<String>();
		List<Branch> branch = new DepartmentService().getBranch();
		List<Postion> postion = new DepartmentService().getPostion();
		String id = request.getParameter("id");
		User editUser = new EditService().getEdit(id);

		HttpSession session = request.getSession();

		if(isValid(request,messages)==true) {
			try {

				String pass = request.getParameter("password");
				String passCheck = request.getParameter("passwordcheck");

				User updateUser = new User();

				updateUser.setId(Integer.parseInt(request.getParameter("id")));
				updateUser.setLogin_id(request.getParameter("login_id"));
				updateUser.setPassword(request.getParameter("password"));
				updateUser.setName(request.getParameter("name"));
				updateUser.setBranch_id(Integer.parseInt(request.getParameter("branch_id")));
				updateUser.setPostion_id(Integer.parseInt(request.getParameter("postion_id")));

				//passwordと確認用のパスワードが空白の場合
				if(StringUtils.isBlank(pass)&&StringUtils.isBlank(passCheck) == true) {
					new EditService().update2(updateUser);//password未変更処理
				} else {
					new EditService().update(updateUser);//password変更処理
				}

			} catch(NoRowsUpdatedRuntimeException e) {
				messages.add("他のユーザによって更新されています。最新のデータを表示しました。データを確認してください。");
				session.setAttribute("errorMessages",messages);
				request.setAttribute("editUser", editUser);
				request.setAttribute("branch", branch);
				request.setAttribute("postion", postion);
				request.getRequestDispatcher("setting.jsp").forward(request, response);
				return ;
			}

			session.setAttribute("editUser", editUser);
			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages",messages);
			request.setAttribute("editUser", editUser);
			request.setAttribute("branch", branch);
			request.setAttribute("postion", postion);
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}
	private boolean isValid(HttpServletRequest request,List<String> messages) {

		String id = request.getParameter("id");
		String login_id = request.getParameter("login_id");
		String password = request.getParameter("password");
		String passwordcheck = request.getParameter("passwordcheck");
		String name = request.getParameter("name");
		String branch_id = String.valueOf(request.getParameter("branch_id"));
		String postion_id = String.valueOf(request.getParameter("postion_id"));

		//未入力時
		if(StringUtils.isBlank(login_id) == true) {
			messages.add("IDを入力してください");
		}
		if(StringUtils.isBlank(name) == true) {
			messages.add("ユーザ名を入力してください");
		}
		if(StringUtils.isBlank(branch_id) == true) {
			messages.add("支店を選択してください");
		}
		if(StringUtils.isBlank(postion_id) == true) {
			messages.add("部署/役職を選択してください");
		}

		//エラー対応
		//半角英数字で6文字以上20文字以下。
    	if(login_id.matches("[0-9a-zA-Z]{6,20}") != true) {
    		messages.add("ログインIDは半角英数字で6文字以上20文字以下で入力してください。");
    	}
    	//名称は10文字以下とします
    	if(name.length()<11 != true) {
    		messages.add("ユーザ名は10文字以下で入力してください。");
    	}

    	//パスワードが入力されていたら
		if(StringUtils.isBlank(password) != true) {

			//記号を含む全ての半角文字で6文字以上20文字以下。
			if(!password.matches("[a-zA-Z0-9｡-ﾟ -/:-@\\[-\\`\\{-\\~]{6,20}")){
				messages.add("パスワードは半角文字で6文字以上20文字以下で入力してください。");
			}
			//確認用のパスワードが未入力だったら
			if(StringUtils.isBlank(passwordcheck) == true) {
				messages.add("確認用のパスワードを入力してください");
			}

	    	//パスワード比較判定
	    	if(!password.equals(passwordcheck)) {
	    		messages.add("パスワードが確認用のパスワードと一致しません。");
	    	}
		}

		//確認用パスワードだけが入力されていたら
		if(StringUtils.isBlank(passwordcheck) != true) {
			if(StringUtils.isBlank(password) == true) {
				messages.add("パスワードを入力してください。");
			}
		}

    	//ログインID重複エラーチェック
		boolean idCheck = new EditService().checkId(id,login_id);

		if(idCheck != true) {
			messages.add("他のユーザによって既にIDは利用されています。");
		}

		if(messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}
}

