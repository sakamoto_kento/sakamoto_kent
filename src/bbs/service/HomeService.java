
package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.User;
import bbs.dao.HomeDao;

public class  HomeService {

	public List<User> getUsers() {
		Connection connection = null;
		try {
			connection = getConnection();

			HomeDao UsersDao = new HomeDao();
			List<User> ret = UsersDao.getUsers(connection);

			commit(connection);
			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User changeStatus(String account_status, String id) {
		Connection connection = null;
		try {
			connection = getConnection();

			HomeDao ChangeDao = new HomeDao();
			User ret = ChangeDao.changeStatus(connection, account_status, id);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}