package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;

import bbs.beans.User;
import bbs.dao.SignupDao;
import bbs.utils.CipherUtil;

public class SignupService {
	//ユーザ登録
	public void register(User user) {

		Connection connection = null;
		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			SignupDao signupdao = new SignupDao();
			signupdao.insert(connection,user);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	//ログインID重複チェック
	public int checkId(String login_id) {

		Connection connection = null;
		try {
			connection = getConnection();

			SignupDao signupdao = new SignupDao();
			int ret = 	signupdao.check(connection,login_id);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
