package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.Branch;
import bbs.beans.Postion;
import bbs.dao.DepartmentDao;

public class DepartmentService {

	public List<Branch> getBranch() {
		Connection connection = null;
		try {
			connection = getConnection();

			DepartmentDao departmentdao = new DepartmentDao();
			List<Branch> ret = departmentdao.getBranch(connection);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			throw e;
		} finally {
			close(connection);
		}
	}

	public List<Postion> getPostion() {
		Connection connection = null;
		try {
			connection = getConnection();

			DepartmentDao departmentdao = new DepartmentDao();
			List<Postion> ret = departmentdao.getPostion(connection);

			commit(connection);

			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			throw e;
		} finally {
			close(connection);
		}
	}
}
