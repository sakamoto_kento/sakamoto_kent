package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;

import bbs.beans.User;
import bbs.dao.EditDao;
import bbs.utils.CipherUtil;

public class EditService {

	public User getEdit(String id) {
		Connection connection = null;
		try {
			connection = getConnection();

			EditDao editdao = new EditDao();
			User ret = editdao.getEditUser(connection, id);

			commit(connection);


			return ret;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update(User editUser) {
		Connection connection = null;

		try {
			connection = getConnection();

			String encPassword = CipherUtil.encrypt(editUser.getPassword());
			editUser.setPassword(encPassword);

			EditDao editdao = new EditDao();
			editdao.update(connection,editUser);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public void update2(User editUser) {
		Connection connection = null;

		try {
			connection = getConnection();

			EditDao editdao = new EditDao();
			editdao.update2(connection,editUser);

			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public boolean checkId(String id,String login_id) {
		Connection connection = null;
		try {
			connection = getConnection();
			EditDao checkDao = new EditDao();
			boolean checkId = checkDao.check(connection, id, login_id);

			commit(connection);
			return checkId;
		} catch(RuntimeException e) {
			rollback(connection);
			throw e;
		} catch(Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
