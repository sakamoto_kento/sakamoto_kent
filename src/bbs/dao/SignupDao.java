package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import bbs.beans.User;
import bbs.exception.SQLRuntimeException;

public class SignupDao {
	public void insert(Connection connection, User user) {
		//ユーザ情報登録
		PreparedStatement  ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO users ( ");
			sql.append("login_id");
			sql.append(", password");
			sql.append(", name");
			sql.append(", branch_id");
			sql.append(", postion_id");
			sql.append(", created_date");
			sql.append(", updated_date");
			sql.append(") VALUES (");
			sql.append(" ?");//login_id
			sql.append(",?");//password
			sql.append(",?");//name
			sql.append(",?");//branch_id
			sql.append(",?");//postion_id
			sql.append(",CURRENT_TIMESTAMP");//created_date
			sql.append(",CURRENT_TIMESTAMP");//updated_date
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getLogin_id());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranch_id());
			ps.setInt(5, user.getPostion_id());

			ps.executeUpdate();
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public int check(Connection connection, String login_id) {
		//新規登録時のログインID重複確認
		PreparedStatement ps = null;
		try {
			String sql = "SELECT login_id FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();

			//入力されたログインIDを検索して該当があったら1をなかったら0を返す
			while(rs.next()) {
				return 1;
			}

			return 0;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}

