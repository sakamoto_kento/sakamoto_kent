package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class EditDao {
	//編集対象ユーザ情報の取得
	public User getEditUser(Connection connection, String id) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//sql
			sql.append("SELECT users.id, ");
			sql.append("users.login_id, ");
			sql.append("users.name, ");
			sql.append("branchs.branch_name, ");
			sql.append("postions.postion_name, ");
			sql.append("users.account_status ");
			sql.append("FROM users ");
			sql.append("Inner JOIN branchs ");
			sql.append("ON users.branch_id = branchs.id ");
			sql.append("Inner JOIN postions ");
			sql.append("ON users.postion_id = postions.id ");
			sql.append("WHERE users.id = ?");
			sql.append("GROUP BY branchs.id ASC");

			ps = connection.prepareStatement(sql.toString());
			ps.setString(1,id);

			ResultSet rs = ps.executeQuery();
			List<User> userList = toUserList(rs);

			if(userList.isEmpty() == true ) {
				return null;
			} else if(2 <= userList.size()) {
				throw new  IllegalStateException("2 <= userList.size()");
			} else {
				return userList.get(0);
			}
		} catch(SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	    	close(ps);
	    }
	}
	private List<User> toUserList(ResultSet rs) throws SQLException {

	    List<User> ret = new ArrayList<User>();
	    try {
	        while (rs.next()) {
	            int id = rs.getInt("id");
	            String login_id = rs.getString("login_id");
	            String name = rs.getString("name");
	            String branch_name = rs.getString("branch_name");
	            String postion_name = rs.getString("postion_name");

	            User user = new User();
	            user.setId(id);
	            user.setLogin_id(login_id);
	            user.setName(name);
	            user.setBranch_name(branch_name);
	            user.setPostion_name(postion_name);

	            ret.add(user);
	        }
	        return ret;
	    } finally {
	        close(rs);
	    }
	}

	public  void update(Connection connection, User editUser) {
		//ユーザ情報変更(pass変更あり)
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", postion_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP ");
			sql.append(" WHERE ");
			sql.append("id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getLogin_id());
			ps.setString(2, editUser.getPassword());
			ps.setString(3, editUser.getName());
			ps.setInt(4, editUser.getBranch_id());
			ps.setInt(5, editUser.getPostion_id());
			ps.setInt(6, editUser.getId());

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public  void update2(Connection connection, User editUser) {
		//ユーザ情報変更(pass変更なし)
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" login_id = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", postion_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP ");
			sql.append(" WHERE ");
			sql.append("id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, editUser.getLogin_id());
			ps.setString(2, editUser.getName());
			ps.setInt(3, editUser.getBranch_id());
			ps.setInt(4, editUser.getPostion_id());
			ps.setInt(5, editUser.getId());

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	public boolean check(Connection connection, String id, String login_id ) {
		PreparedStatement ps = null;
		try {
			String sql = "SELECT id FROM users WHERE login_id = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, login_id);

			ResultSet rs = ps.executeQuery();
			//結果をユーザIDで返して、引数のidと同一かをチェックをする。

			String ret = null;
			while(rs.next()) {
				ret = rs.getString("id");
				//同一だったら処理を通すためtrue 相違していたら処理は通さないためfalse
				if(ret.equals(id)) {
					return true;
				}
				return false;
			}
			//nullだったら同じログインIDは登録されていない為true
			return true;
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
}


