package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.Branch;
import bbs.beans.Postion;
import bbs.exception.SQLRuntimeException;

public class DepartmentDao {
	//支店ID,支店名
	public List<Branch> getBranch(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//SQL
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("branch_name ");
			sql.append("FROM Branchs ");
			sql.append("  GROUP BY id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Branch> ret = toBranch(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private  List<Branch> toBranch(ResultSet rs)
		throws SQLException {

		List<Branch> ret = new ArrayList<Branch>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String branch_name = rs.getString("branch_name");

				Branch branch = new Branch();
				branch.setId(id);
				branch.setBranch_name(branch_name);

				ret.add(branch);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	//部署・役職ID、部署・役職名
	public List<Postion> getPostion(Connection connection ) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			//SQL
			sql.append("SELECT ");
			sql.append("id, ");
			sql.append("postion_name ");
			sql.append("FROM postions ");
			sql.append("  GROUP BY id ASC");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<Postion> ret = toPostion(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<Postion> toPostion(ResultSet rs)
		throws SQLException {

		List<Postion> ret = new ArrayList<Postion>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String postion_name = rs.getString("postion_name");

				Postion postion = new Postion();
				postion.setId(id);
				postion.setPostion_name(postion_name);

				ret.add(postion);
			}
			return ret;
		} finally {
			close(rs);
		}
	}
}