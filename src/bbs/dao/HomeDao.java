package bbs.dao;

import static bbs.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import bbs.beans.User;
import bbs.exception.NoRowsUpdatedRuntimeException;
import bbs.exception.SQLRuntimeException;

public class HomeDao {
	//ユーザ情報一覧取得
	public List<User> getUsers(Connection connection) {
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT users.id, ");
			sql.append("users.login_id, ");
			sql.append("users.name, ");
			sql.append("branchs.branch_name, ");
			sql.append("postions.postion_name, ");
			sql.append("users.created_date, ");
			sql.append("users.updated_date, ");
			sql.append("users.account_status ");
			sql.append("FROM users ");
			sql.append("Inner JOIN branchs ");
			sql.append("ON users.branch_id = branchs.id ");
			sql.append("Inner JOIN postions ");
			sql.append("ON users.postion_id = postions.id");

			ps = connection.prepareStatement(sql.toString());

			ResultSet rs = ps.executeQuery();
			List<User> ret = toUsers(rs);
			return ret;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<User> toUsers(ResultSet rs)
		throws SQLException {

		List<User> ret = new ArrayList<User>();
		try {
			while(rs.next()) {
				int id = rs.getInt("id");
				String login_id =  rs.getString("login_id");
				String name = rs.getString("name");
				String	branch_name = rs.getString("branch_name");
				String postion_name = rs.getString("postion_name");
				int account_status = rs.getInt("account_status");
				Date created_date = rs.getDate("created_date");
				Date updated_date = rs.getDate("updated_date");

				User users = new User();
				users.setId(id);
				users.setLogin_id(login_id);
				users.setName(name);
				users.setBranch_name(branch_name);
				users.setPostion_name(postion_name);
				users.setAccount_status(account_status);
				users.setCreated_date(created_date);
				users.setUpdated_date(updated_date);

				ret.add(users);
			}
			return ret;
		} finally {
			close(rs);
		}
	}

	public User changeStatus(Connection connection, String account_status, String id) {
		//アカウントステータス切り替え
		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();

			sql.append("UPDATE users SET");
			sql.append(" account_status = ?");
			sql.append(" WHERE ");
			sql.append("id = ?");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, account_status);
			ps.setString(2, id);

			int count = ps.executeUpdate();
			if(count == 0) {
				throw new NoRowsUpdatedRuntimeException();
			}
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
		return null;
	}
}