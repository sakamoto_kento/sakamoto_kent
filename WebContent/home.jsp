<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>ホーム画面</title>
	</head>
	<body>
		<div class="main-contents">
			<div class="header">
				<a href="signup">新規登録</a>
			</div>
			<div class="userlist">
				<table border="1">
					<tr>
						<th>ログインID</th>
						<th>ユーザネーム</th>
						<th>支店</th>
						<th>部署・役職</th>
						<th>登録日</th>
						<th>更新日</th>
						<th>登録情報編集</th>
					</tr>
					<c:forEach items="${users}" var="users">
						<tr>
							<td class="login_id"><c:out value="${users.login_id}" /></td>
							<td class="name"><c:out value="${users.name}" /></td>
							<td class="branch_name"><c:out value="${users.branch_name }" /></td>
							<td class="postion_name"><c:out value="${users.postion_name }" /></td>
							<td class="created_date"><fmt:formatDate value="${users.created_date }" pattern="yyyy/MM/dd" /></td>
							<td class="updated_date"><fmt:formatDate value="${users.updated_date }" pattern="yyyy/MM/dd" /></td>
							<td class="setting">
							<form action="setting" method="GET">
								<input type="hidden" name="login_id" value="${users.login_id }" />
								<input type="hidden" name="id" value="${users.id }" />
								<input type="submit" value="編集" />
							</form>
							<form action= "index.jsp" method="POST">
								<c:choose>
									<c:when test="${users.account_status == 1}">
										<input type="hidden" name="account_status" value=0>
										<input type="hidden" name="id" value="${users.id}">
   							 			<input type="submit" value="停止" onClick="return confirm('選択したアカウントの停止を行います。本当によろしいですか？')">
<!-- 										<input type="submit" value="停止" onClick="return cofirm('選択したアカウントの停止を行います。本当によろしいですか？')">  -->
									</c:when>
									<c:when test="${users.account_status == 0}">
		   							 	<input type="hidden" name="account_status" value=1>
   							 			<input type="hidden" name="id" value="${users.id}">
   							 			<input type="submit" value="復活" onClick="return confirm('選択したアカウントの復活を行います。本当によろしいですか？')">
									</c:when>
								</c:choose>
							</form>
							</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<div class="copyright">Copyright(c)Sakamoto Kento</div>
		</div>
	</body>
</html>