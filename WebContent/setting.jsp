<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    	<title>${editUser.login_id }ユーザの設定</title>
	</head>
	<body>
		<div class="main-content">
    		<c:if test = "${ not empty errorMessages }">
    			<div class = "errorMessages">
    				<ul>
    					<c:forEach items="${errorMessages}" var="messages">
    						<li><c:out value="${messages}"/>
    					</c:forEach>
    				</ul>
    			</div>
    			<c:remove var="errorMessages" scope="session"/>
    		</c:if>
    		<form action="setting" method="POST">
    			<input type="hidden" name="id" value="${editUser.id }">
    			<span class="login_id"><c:out value="${editUser.login_id }の編集画面" /></span><br>

    			<label class="login_id">ログインID</label><br>
    			<input name="login_id" id="login_id" value="${editUser.login_id }" /><br>

    			<label class="password">パスワード</label><br>
    			<input name="password" id="password" type="password" /><br>

    			<label class="passewordcheck">パスワード(確認)</label><br>
    			<input name="passwordcheck" id="passwordcheck" type="password" /><br>

    			<label class="name">ユーザ名</label><br>
    			<input name="name" id="name" value="${editUser.name }" /><br>

				<label class="branch">支店</label><br>
				<select name="branch_id" id ="branch_id">
					<c:forEach items="${branch}" var="branch">
						<c:choose>
							<c:when test="${editUser.branch_name == branch.branch_name}">
								<option value="${branch.id}" selected>${branch.branch_name}</option>
							</c:when>
							<c:when test="${editUser.branch_name != branch.branch_name}">
								<option value="${branch.id}">${branch.branch_name}</option>
							</c:when>
						</c:choose>
			 		</c:forEach>
				</select><br>

				<label class="branch">部署</label><br>
				<select name="postion_id" id ="postion_id">
					<c:forEach items="${postion}" var="postion">
						<c:choose>
							<c:when test="${editUser.postion_name == postion.postion_name}">
   								<option value="${postion.id}" selected>${postion.postion_name}</option>
  							</c:when>
							<c:when test="${editUser.postion_name != postion.postion_name}">
    							<option value="${postion.id}">${postion.postion_name}</option>
  							</c:when>
						</c:choose>
			 		</c:forEach>
				</select>
			<input type="submit" value="変更" /><br>
    	</form>
    <a href="./">戻る</a><br>
    	<div class="copyright">Copyright(c)Sakamoto Kento</div>
    	</div>
	</body>
</html>