<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>新規登録画面</title>
	</head>
	<body>
		<div class="main-contents">
			<c:if test="${not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages }" var="message" >
							<li><c:out value="${message }" />
						</c:forEach>
					</ul>
				</div>
					<c:remove var="errorMessages" scope="session" />
			</c:if>
			<form action="signup" method="POST">
				<label for="login_id">ログインID</label><br>
					<input name="login_id" id="login_id" value="${user.login_id}" /><br>
				<label for="password">パスワード</label><br>
					<input name="password" id="password" type="password" value="${user.password }" /><br>
				<label for="passwordcheck">確認用パスワード</label><br>
					<input name="passwordcheck" id="passwordcheck" type="password" /><br>
				<label for="name">ユーザ名</label><br>
				<input name="name" id="name" value="${user.name }" /><br>

				<label class="branch">支店</label><br>
					<select name="branch_id" id="branch_id">
						<c:forEach items="${branch}" var="branch">
							<c:choose>
								<c:when test="${user.branch_id == branch.id }">
									<option value="${branch.id }" selected >${branch.branch_name }</option>
								</c:when>
								<c:when test="${user.branch_id != branch.id }">
									<option value="${branch.id }">${branch.branch_name }</option>
								</c:when>
							</c:choose>
						</c:forEach>
					</select><br>
				<label class="postion">部署</label><br>
				<select name="postion_id" id="postion_id">
					<c:forEach items="${postion}" var="postion">
						<c:choose>
							<c:when test="${user.postion_id == postion.id }">
								<option value="${postion.id }" selected >${postion.postion_name }</option>
							</c:when>
							<c:when test="${user.postion_id != postion.id }">
								<option value="${postion.id }">${postion.postion_name }</option>
							</c:when>
						</c:choose>
					</c:forEach>
				</select><br>
			<input type="submit" value="登録" /><br><a href="./">戻る</a>
			</form>
			<div class="copyrigth"> Copyright(c)Sakamoto Kento</div>
		</div>
	</body>
</html>